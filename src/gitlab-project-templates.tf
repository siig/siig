# Contains the base infrastructure for the entire Siig group
resource "gitlab_project" "templates" {
  name                   = "templates"
  description            = "All templates related to DevOps work within the siig homelab"
  visibility_level       = terraform.workspace == "prod" ? "public" : "private"
  default_branch         = "main"
  shared_runners_enabled = true
  namespace_id           = gitlab_group.homelab.id
}

########################
## BRANCH PROTECTIONS ##
########################

resource "gitlab_branch_protection" "main_main" {
  project            = gitlab_project.templates.id
  branch             = "main"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "main_devel" {
  project            = gitlab_project.templates.id
  branch             = "devel"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}
