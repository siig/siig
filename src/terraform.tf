terraform {
  required_version = ">= 0.12"

  backend "remote" {
    organization = "siig"

    workspaces {
      prefix = "siig-"
    }
  }
}

resource "random_integer" "project_id" {
  min = 001
  max = 999
}

provider "gitlab" {
  alias = "main"
  version = "~> 2.10"
}
