# Contains dev repos and groups
resource "gitlab_group" "dev" {
  count       = terraform.workspace == "prod" ? 0 : 1
  name        = "dev"
  path        = "dev"
  description = "Contains dev version of the parent group to this group"
  parent_id   = 8345471
}

# Group for homelab bounded context
resource "gitlab_group" "homelab" {
  name             = "homelab"
  path             = "homelab"
  description      = "Bounded context for homelab"
  parent_id        = local.main_group
  visibility_level = terraform.workspace == "prod" ? "public" : "private"
}
