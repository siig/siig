variable "gitlab_token" {
  description = "Token to authorize with GitLab"
}

locals {
  main_group = terraform.workspace == "prod" ? 8345471 : gitlab_group.dev[0].id
}
