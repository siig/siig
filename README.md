# Siig

This repository creates the basis for everything related to my technical life. Whether it be the configurations I have for my shell and my programs, to the scripts I use to setup my workstations, you will be able to find it here.

# Does that mean your entire life is public?

Well not quite. You can go to my to my [root group on GitLab](https://gitlab.com/siig) and find that I'm using GitLab as a second brain. I use it for my notes. I use it for my scripts. I even use it for my shopping lists, and keeping track of the things I want done around the house! Of course those last things are not so relevant to the public, so those repos are private.

# But why?

This whole concept may indeed seem weird to some, and yeah I suppose it is... Well that's never stopped me before! My theory is that this will help de-clutter my life, and in the end will make it easier. My goal is that I will never have to spend an hour googling "How to do XYZ" once someone asks how I setup something cool somewhere. All my (useful) knowledge should be contained within here.